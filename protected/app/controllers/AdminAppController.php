<?php

class AdminAppController extends \BaseController {

	public function index(){
		return View::make('admin.pages.dashboard')
				->withTitle('Dashboard');
	}

	public function login(){
		return View::make('admin.pages.auth.login')
				->withTitle('Login');
	}

	public function dologin(){
		$rules = array(
			'email'    => 'required|email',
			'password' => 'required|alphaNum|min:5'
		);
 
		$validator = Validator::make(Input::all(), $rules);
	 
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator)
				->withInput(Input::except('password'));
		} else {
			try
			{
				$credentials = array(
					'email'     => Input::get('email'),
					'password'  => Input::get('password')
				);
				$remember = false;
				$remember = Input::get('remember');
				// digunakan untuk login
				$user = Sentry::authenticate($credentials, $remember);
				if($user){
					$customer = Sentry::findUserByLogin(Input::get('email'));
					return Redirect::to('admin')->with('success','login berhasil');
				}
	 
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
				Session::flash('error', 'Login field is required.');
				return Redirect::to('admin/login')
					->withInput(Input::except('password'))
					->with('error','Login field is required.');
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
				Session::flash('error', 'Password field is required.');
				return Redirect::to('admin/login')
					->withInput(Input::except('password'))
					->with('error','Password field is required.');
			}
			catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
			{
				Session::flash('error', 'Wrong password, try again.');
				return Redirect::to('admin/login')
					->withInput(Input::except('password'))
					->with('error','Wrong password, try again.');
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				Session::flash('error', 'User was not found.');
				return Redirect::to('admin/login')
					->withInput(Input::except('password'))
					->with('error','User was not found.');
			}
			catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
			{
				Session::flash('error', 'User is not activated.');
				return Redirect::to('admin/login')
					->withInput(Input::except('password'))
					->with('error','User is not activated.');
			}
	 
			// The following is only required if the throttling is enabled
			catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
			{
				Session::flash('error', 'User is suspended.');
				return Redirect::to('admin/login')
					->withInput(Input::except('password'))
					->with('error','User is suspended.');
			}
			catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
			{
				Session::flash('error', 'User is banned.');
				return Redirect::to('admin/login')
					->withInput(Input::except('password'))
					->with('error','User is banned.');
			}
		}
	}

	public function logout(){
		Sentry::logout();
		return Redirect::to('admin/login')->with('success','Logout Berhasil');
	}

	public function forgotPassword(){
		return View::make('admin.pages.auth.forgotPassword')
				->withTitle('Forgot Password');
	}

	public function sendForgotPassword(){
		try{
			$email = Input::get('email');
			$user = Sentry::findUserByLogin($email);
			$resetCode = $user->getResetPasswordCode();
			Mail::send('admin.pages.auth.resetCode', array('code'=>$resetCode,'idUser'=>Crypt::encrypt($user->id)), function($message){
				$message->to(Input::get('email'))->subject('Reset Password');
			});
			return Redirect::to('admin/login');
		}catch (Cartalyst\Sentry\Users\UserNotFoundException $e){
			Session::flash('error', 'User was not found.');
				return Redirect::to('admin/login')
					->withInput(Input::except('password'))
					->with('error','User was not found.');
		}
	}

	public function viewFormForgotPassword($code, $id){
		$data = [
			'code'	=> $code,
			'id'	=> $id
		];
		return View::make('admin.pages.auth.formForgotPassword', $data)
				->withTitle('Forgot Password');
	}

	public function doForgotPassword(){
		$rules = array(
			'password' => 'required|between:4,11',
			'confirmPassword' => 'between:4,11|same:password',
		);
	 
		$validator = Validator::make(Input::all(), $rules);
	 
		if ($validator->fails()) {  
			return Redirect::to('admin/login/forgotPassword/'.Input::get('code').'/'.Input::get('id'))->withErrors($validator)->withInput();
		} 
		try{
			$user = Sentry::findUserById(Crypt::decrypt(Input::get('id')));

			if ($user->checkResetPasswordCode(Input::get('code')))
			{
				if ($user->attemptResetPassword(Input::get('code'), Input::get('password')))
				{
					return Redirect::to('admin/login');
				}
				else
				{
					// Password reset failed
				}
			}
			else
			{
				// The provided password reset code is Invalid
			}
		}catch (Cartalyst\Sentry\Users\UserNotFoundException $e){
			echo 'User was not found.';
		}
	}
}
