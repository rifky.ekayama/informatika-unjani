<?php

class BlogPostsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$posts = Post::all();
		//$posts = Post::orderBy('id','desc')->paginate(10);
		return View::make('admin.pages.blogging.index',compact('posts'))
				->withTitle('Blog / Articles List');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('admin.pages.blogging.create')
				->withTitle('Create Articles');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$validator = Validator::make(Input::all(), Post::$rules);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		} else {
			if(!empty(Input::file('file'))){
				$dir = 'assets/uploads/image_articles/';
				$file = Input::file('file');
				$file->move($dir, $file->getClientOriginalName());
				$filename = $dir.$file->getClientOriginalName();
			}

			$post = new Post();
			$post->user_id 			= Sentry::getUser()->id;
			$post->title 			= Input::get('title');
			$post->content 			= Input::get('content');
			$post->read_more		= ((strlen($post->content) > 120) ? substr($post->content, 0, 120) : $post->content);
			$post->comment_count 	= 0;
			$post->file 			= (!empty($filename) ? $filename : '');
			$post->status 			= Input::get('status');
			$post->categories		= json_encode(Input::get('categories'));
			$post->tags 			= json_encode(Input::get('tags'));
			$post->metaTitle		= Input::get('metaTitle');
			$post->metaDesc			= Input::get('metaDesc');
			$post->metaKeyword		= Input::get('metaKeyword');
			$post->save();
			return Redirect::to('admin/blog/articles')->with('success', 'Post is saved!');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$comments = $post->comments()->where('approved', '=', 1)->get();
		$this->layout->title = $post->title;
		$this->layout->main = View::make('home')->nest('content', 'posts.single', compact('post', 'comments'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		return View::make('admin.pages.blogging.edit')
				->withTitle('Edit Blog / Articles');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$data = [
			'title' => Input::get('title'),
			'content' => Input::get('content'),
		];
		$rules = [
			'title' => 'required',
			'content' => 'required',
		];
		$valid = Validator::make($data, $rules);
		if ($valid->passes())
		{
			$post->title = $data['title'];
			$post->content = $data['content'];
			$post->read_more = (strlen($post->content) > 120) ? substr($post->content, 0, 120) : $post->content;
			if(count($post->getDirty()) > 0) /* avoiding resubmission of same content */
			{
				$post->save();
				return Redirect::back()->with('success', 'Post is updated!');
			}
			else
				return Redirect::back()->with('success','Nothing to update!');
		}
		else
			return Redirect::back()->withErrors($valid)->withInput();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$post->delete();
		return Redirect::route('post.list')->with('success', 'Post is deleted!');
	}


}
