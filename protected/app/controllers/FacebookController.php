<?php

class FacebookController extends \BaseController{

	private $fb;

	public function __construct(FacebookHelper $fb){
		$this->fb = $fb;
	}

	public function index()
	{
		//
		$data = array();
		if (Auth::check()) {
			$data = Auth::user();
		}

		if(Sentry::check()){
			return View::make('admin.pages.dashboard', array('data'=>$data));
		}else{
			return View::make('admin.pages.auth.login', array('data'=>$data));
		}
	}

	public function login(){
		return Redirect::to($this->fb->getUrlLogin(Config::get('facebook.app_scope')));
	}

	public function callback(){
		if( !$this->fb->generateSessionFromRedirect()){
			return Redirect::to('/')->with('message','Error koneksi ke facebook');
		}

		$user_fb = $this->fb->getGraph();
		echo "<pre>";
		//dd($user_fb);
		echo "</pre>";
		if(empty($user_fb)){
			return Redirect::to('/')->with('message', 'Gagal mengambil data dari facebook');
		}

		$user = User::where('uidfb','=',$user_fb->getProperty('id'))->first();
		//dd($user);
		if(is_null($user)){
			$user =  new User;
			$user->email = $user_fb->getProperty('email');
			$user->first_name = $user_fb->getProperty('first_name');
			$user->last_name = $user_fb->getProperty('last_name');
			$user->full_name = $user_fb->getProperty('name');
			$user->photo = 'http://graph.facebook.com/'.$user_fb->getProperty('id').'/picture?type=large';
			$user->uidfb = $user_fb->getProperty('id');
			$user->access_token = $this->fb->getToken();
			$user->save();
		}
	}

	public function post($desc, $link){
		$newsession = $this->fb->generateSessionFromToken(User::where('email','=','rifqykiw@gmail.com')->first()->access_token);
		dd($this->fb->postLinkToTimeline($newsession,$desc,$link));
	}
	public function read(){
		$newsession = $this->fb->generateSessionFromToken(User::where('email','=','rifqykiw@gmail.com')->first()->access_token);
		echo "<pre>";
		dd($this->fb->readTimeline($newsession));
		echo "</pre>";
	}

	// public function comment($object_id, $message){
	// 	$newsession = $this->fb->generateSessionFromToken(User::where('email','=','rifqykiw@gmail.com')->first()->access_token);
	// 	echo "<pre>";
	// 	dd($this->fb->commentPost($newsession, $object_id, $message));
	// 	echo "</pre>";
	// }

	public function comment(){
		return View::make('comment');
	}

	public function readComment($object_id){
		$newsession = $this->fb->generateSessionFromToken(User::where('email','=','rifqykiw@gmail.com')->first()->access_token);
		echo "<pre>";
		dd($this->fb->readComment($newsession, $object_id));
		echo "</pre>";
	}
}
