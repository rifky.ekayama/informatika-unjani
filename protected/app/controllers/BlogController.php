<?php

class BlogController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$posts = Post::orderBy('id','desc')->paginate(10);
		return View::make('admin.pages.blogging.dashboard', compact($posts))
				->withTitle('Blog Dashboard');
	}

	public function getSearch()
	{
		$searchTerm = Input::get('s');
		$posts = Post::whereRaw('match(title,content) against(? in boolean mode)',[$searchTerm])
					 ->paginate(10);
		$posts->appends(['s'=>$searchTerm]);
		return View::make('home')->nest('content','index',($posts->isEmpty()) ? ['notFound' => true ] : compact('posts'));
	}

}
