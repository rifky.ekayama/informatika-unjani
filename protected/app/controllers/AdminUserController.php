<?php

class AdminUserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$user = User::paginate(10);
		$user = ['user' => $user];
		return View::make('admin.pages.users.index', $user)
				->withTitle('Users');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$group = Group::lists('name', 'id');
		$data = ['group' => $group];
		return View::make('admin.pages.users.create', $data)
				->withTitle('Create User');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$validator = Validator::make(Input::all(), User::$rules);
	 
		if ($validator->fails()) {  
			return Redirect::to('admin/users/users/create')->withErrors($validator)->withInput();
		} else {           
			 
			try
			{
				$user = Sentry::createUser(array(
					'first_name'	=> Input::get('firstName'),
					'last_name' 	=> Input::get('lastName'),
					'email' 		=> Input::get('email'),
					'password' 		=> Input::get('password'),
					'activated' 	=> true,
				));
	 
				$groupbyid = Sentry::findGroupById(Input::get('group'));
				$user->addGroup($groupbyid);
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
				Session::flash('error', 'Login field is required.');
				return Redirect::to('admin/users/users');
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
				Session::flash('error', 'Password field is required.');
				return Redirect::to('admin/users/users');
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
				Session::flash('error', 'User with this login already exists.');
				return Redirect::to('admin/users/users');
			}
			catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
			{
				Session::flash('error', 'Group was not found.');
				return Redirect::to('admin/users/users');
			}
		Session::flash('success', 'Data Berhasil Ditambahkan');
		return Redirect::to('admin/users/users');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$userbyid = Sentry::findUserByID(Crypt::decrypt($id));
		$groupbyuser = $userbyid->getGroups();
		$groupbyuser = json_decode($groupbyuser, true);
		$group = Group::lists('name', 'id');
		$data =
		[
			'userbyid' => $userbyid,
			'group' => $group,
			'groupbyuser' => $groupbyuser[0]['id']
		];
		return View::make('admin.pages.users.edit', $data)
				->withTitle('Edit User');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$validator = Validator::make(Input::all(), User::$rules);
	 
		if ($validator->fails()) {  
			return Redirect::to('admin/users/users/'.Crypt::encrypt(Crypt::decrypt($id)).'/edit')->withErrors($validator)->withInput();
		} else {
			try
			{
			$user 				= Sentry::findUserById(Crypt::decrypt($id));
			$groupbyuser 		= $user->getGroups();
			$groupbyuser 		= json_decode($groupbyuser, true);
			$groupbyuser 		= Sentry::findGroupById($groupbyuser[0]['id']);
			$user->removeGroup($groupbyuser);
	 
			$groupbyuser		 = Sentry::findGroupById(Input::get('group'));
			$user->addGroup($groupbyuser);
			 
			$user->first_name	= Input::get('firstName');
			$user->last_name 	= Input::get('lastName');
			$user->email 		= Input::get('email');
			$user->password 	= Input::get('password');
	 
			if ($user->save())
			{
				Session::flash('success', 'Data Berhasil Ditambahkan');
				return Redirect::to('admin/users/users');
			}
			else
			{
				Session::flash('success', 'Data Gagal Ditambahkan');
				return Redirect::to('admin/users/users');
			}
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
				Session::flash('error', 'User with this login already exists.');
				return Redirect::to('admin/users/users');
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				Session::flash('error', 'User was not found.');
				return Redirect::to('admin/users/users');
			}
			catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
			{
				Session::flash('error', 'Group was not found.');
				return Redirect::to('admin/users/users');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		try
		{
			$user = Sentry::findUserById(Crypt::decrypt($id));
			$user->delete();
		 }
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			Session::flash('error', 'User was not found.');
			return Redirect::to('admin/users/users');
		}
		Session::flash('success', 'Data Berhasil Dihapus');
		return Redirect::to('admin/users/users');
	}


}
