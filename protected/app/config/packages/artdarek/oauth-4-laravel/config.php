<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Facebook
		 */
		'Facebook' => array(
			'client_id'     => '592565097541408',
			'client_secret' => '87d5ded7dc4870491eda459d05c37d6c',
			'scope'         => array('user_about_me',
								 'user_actions.music',
								 'user_birthday',
								 'user_friends',
								 'user_hometown',
								 'user_managed_groups',
								 'user_relationship_details',
								 'user_status',
								 'user_website',
								 'user_actions.books',
								 'user_actions.news',
								 'user_education_history',
								 'user_games_activity',
								 'user_likes',
								 'user_photos',
								 'user_relationships',
								 'user_tagged_places',
								 'user_work_history',
								 'user_actions.fitness',
								 'user_actions.video',
								 'user_events',
								 'user_groups',
								 'user_location',
								 'user_posts',
								 'user_religion_politics',
								 'user_videos',
								 'manage_notifications',
								 'publish_pages',
								 'read_mailbox',
								 'email',
								 'publish_actions'),
		),
		
		/**
		*Google
		*/
		'Google' => array(
			'client_id'     => 'Your Google client ID',
			'client_secret' => 'Your Google Client Secret',
			'scope'         => array('userinfo_email', 'userinfo_profile'),
		),

		/**
		*Twitter
		*/
		'Twitter' => array(
		    'client_id'     => 'QApnGFVcrkgApbwBRD8UXmelf',
		    'client_secret' => 'Rz77ycwvt16b77dZlfJOzS8L3qwp5cm5t5IaWq8daK3TktRhRM',
		    // No scope - oauth1 doesn't need scope
		),

		/**
		*Linkedin
		*/
		'Linkedin' => array(
		    'client_id'     => 'Your Linkedin API ID',
		    'client_secret' => 'Your Linkedin API Secret',
		),

		/**
		*Yahoo
		*/
		'Yahoo' => array(
            'client_id'     => 'Your Yahoo API KEY',
            'client_secret' => 'Your Yahoo API Secret',  
		),
	)

);