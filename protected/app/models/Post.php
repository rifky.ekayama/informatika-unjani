<?php
// file: app/models/Post.php
class Post extends Eloquent {

	protected $table = 'posts';

	public static $rules = [
		// 'title' => 'required'
		'title'			=> 'required',
		'content'		=> 'required',
		'status'		=> 'required',
		'categories'	=> 'required',
		'tags'			=> 'required',
	];

	protected $fillable = ['user_id','title','read_more','content','comment_count','file','status','categories','tags','metaTitle' ,'metaDesc','metaKeyword'];
 
    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function user(){
    	return $this->belongsTo('User', 'user_id');
    }
 
}