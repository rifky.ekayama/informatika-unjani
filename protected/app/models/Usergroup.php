<?php
	class Usergroup extends Eloquent{
		protected $table = 'users_groups';

		public function user(){
			return $this->belongsTo('User', 'user_id');
		}

		public function group(){
			return $this->belongsTo('Group', 'group_id');
		}
	}