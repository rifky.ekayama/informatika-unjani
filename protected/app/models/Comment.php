<?php
// file: app/models/Comment.php
class Comment extends Eloquent {

	protected $table = 'comments';

	public static $rules = [
		// 'title' => 'required'
	];
 
    public function post()
    {
        return $this->belongsTo('Post');
    }
}