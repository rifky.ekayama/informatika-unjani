<?php
	class Group extends Eloquent{
		protected $table = 'groups';

		public static $rules = [
			// 'title' => 'required'
			'name' => 'required',
		];

		public function user(){
			return $this->hasMany('Usergroup', 'group_id');
		}
	}