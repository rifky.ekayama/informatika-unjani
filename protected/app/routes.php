<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(array('prefix' => 'admin'), function(){
	Route::get('logout', 'AdminAppController@logout');
	Route::group(array('prefix' => 'login'), function(){
		Route::get('/', 'AdminAppController@login');
		Route::post('/', 'AdminAppController@dologin');
		Route::get('forgotPassword', 'AdminAppController@forgotPassword');
		Route::post('forgotPassword', 'AdminAppController@sendForgotPassword');
		Route::get('forgotPassword/{code}/{id}', 'AdminAppController@viewFormForgotPassword');
		Route::post('doForgotPassword', 'AdminAppController@doForgotPassword');
	});
	Route::group(array('before' => 'sentry_auth'), function(){
		Route::get('/', 'AdminAppController@index');
		Route::group(array('prefix' => 'users'), function(){
			Route::resource('groups', 'AdminGroupController');
			Route::get('groups/delete/{id}', 'AdminGroupController@destroy');
			Route::resource('users', 'AdminUserController');
			Route::get('users/delete/{id}', 'AdminUserController@destroy');
		});
		Route::group(array('prefix' => 'blog'), function(){
			Route::get('/', 'BlogController@index');
			Route::resource('articles', 'BlogPostsController');
			Route::get('articles/delete/{id}', 'BlogPostsController@destroy');
		});
	});
});

// Route::get('/', 'FacebookController@index');
// Route::get('logout', 'FacebookController@logout');
// Route::get('login/fb', 'FacebookController@login');
// Route::group(array('prefix' => 'facebook'), function(){
// 	Route::get('login/callback', 'FacebookController@callback');
// 	Route::get('post/{desc}/{link}', 'FacebookController@post');
// 	Route::get('read','FacebookController@read');
// 	Route::get('comment', 'FacebookController@comment');
// 	Route::get('read/comment/{object_id}', 'FacebookController@readComment');
// });
// Route::group(array('prefix' => 'twitter'), function(){
// 	Route::get('post/{status}', 'TwitterController@postTwit');
// 	Route::get('mention', 'TwitterController@readMention');
// });