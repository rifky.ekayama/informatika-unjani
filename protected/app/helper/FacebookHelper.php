<?php

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;

class FacebookHelper{

	private $helper;
	private $session;

	public function __construct(){
		FacebookSession::setDefaultApplication(Config::get('facebook.app_id'), Config::get('facebook.app_secret'));
		$this->helper = new FacebookRedirectLoginHelper(url('facebook/login/callback'));
	}

	public function getUrlLogin($scope){
		return $this->helper->getLoginUrl($scope);
	}

	public function getUrlLogout($session, $next_url){
		return $this->helper->getLogoutUrl($session, $next_url);
	}

	public function generateSessionFromRedirect(){
		$this->session = null;
		try {
			$this->session = $this->helper->getSessionFromRedirect();
		} catch(FacebookRequestException $ex) {
		  // When Facebook returns an error
		} catch(\Exception $ex) {
		  // When validation fails or other local issues
		}

		return $this->session;
	}

	public function generateSessionFromToken($token){
		$this->session = new FacebookSession($token);
		return $this->session;
	}

	public function getGraph(){
		$request = new FacebookRequest($this->session, 'GET', '/me');
		$response = $request->execute();
		return $response->getGraphObject();
	}

	public function getToken(){
		return $this->session->getToken();
	}

	public function postLinkToTimeline($session, $desc, $link){
		$request = new FacebookRequest($session, 'POST', '/me/feed', array(
						'message' => $desc,
						'link' => $link,
			)
		);
		$response = $request->execute();
		return $response->getGraphObject();
	}

	public function readTimeline($session){
		$request = new FacebookRequest($session, 'GET', '/me/feed');
		$response = $request->execute();
		return $response->getGraphObject();	
	}

	public function commentPost($session, $object_id, $message){
		$request = new FacebookRequest($session, 'POST', '/'.$object_id.'/comments', array(
				'message' => $message,
  			)
		);
		$response = $request->execute();
		return $response->getGraphObject();
	}

	public function readComment($session, $object_id){
		$request = new FacebookRequest( $session, 'GET', '/'.$object_id.'/comments');
		$response = $request->execute();
		return $response->getGraphObject();
	}
}