<?php

class TwitterHelper{

	public function postTwit($status){
		return Twitter::postTweet(['status' => $status, 'format' => 'json']);
	}

	public function readMention(){
		return Twitter::getMentionsTimeline(array('count' => 20, 'format' => 'json'));
	}

	public function readHomeTimeline(){
		return Twitter::getHomeTimeline(array('count' => 20, 'format' => 'json'));
	}

	public function readUserTimeline(){
		return Twitter::getUserTimeline(array('screen_name' => 'thujohn', 'count' => 20, 'format' => 'json'));
	}
}