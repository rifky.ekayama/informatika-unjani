<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->

<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<!-- BEGIN META SECTION -->
	<meta charset="utf-8">
	<title>
		@yield('title')
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta content="" name="description" />
	<meta content="themes-lab" name="author" />
	<!-- END META SECTION -->
	<!-- BEGIN MANDATORY STYLE -->
	{{ HTML::style('assets/admin/css/icons/icons.min.css') }}
	{{ HTML::style('assets/admin/css/bootstrap.min.css') }}
	{{ HTML::style('assets/admin/css/plugins.min.css') }}
	{{ HTML::style('assets/admin/css/style.min.css') }}
	{{ HTML::style('assets/admin/css/animate-custom.css') }}
	<!-- END  MANDATORY STYLE -->
	<!-- BEGIN PAGE LEVEL STYLE -->
	{{ HTML::style('assets/admin/plugins/noty/themes/animate.css') }}
	{{ HTML::style('assets/admin/plugins/fullcalendar/fullcalendar.css') }}
	{{ HTML::style('assets/admin/plugins/metrojs/metrojs.css') }}
	{{ HTML::style('assets/admin/plugins/datetimepicker/jquery.datetimepicker.css') }}
	{{ HTML::style('assets/admin/plugins/pickadate/themes/default.css') }}
	{{ HTML::style('assets/admin/plugins/pickadate/themes/default.date.css') }}
	{{ HTML::style('assets/admin/plugins/pickadate/themes/default.time.css') }}
	{{ HTML::style('assets/admin/plugins/datatables/dataTables.css') }}
	{{ HTML::style('assets/admin/plugins/datatables/dataTables.tableTools.css') }}
	{{ HTML::style('assets/admin/plugins/icheck/skins/all.css') }}
	{{ HTML::style('assets/admin/plugins/dropzone/dropzone.css') }}
	<!-- END PAGE LEVEL STYLE -->
	{{ HTML::script('assets/admin/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js') }}
</head>

<body class="{{ Request::is('admin/login/*') ? 'login fade-in' : 'dashboard' }}" >
	@if(Sentry::check())
		@include('admin.includes.navbar')
		<!-- BEGIN WRAPPER -->
		<div id="wrapper">
			@include('admin.includes.sidebar')
			<!-- BEGIN MAIN CONTENT -->
			<div id="main-content" class="{{ Request::is('admin') ? 'dashboard' : '' }}">
				@yield('content')
				@yield('error')
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END WRAPPER -->
		@include('admin.includes.chatmenu')
	@else
		@yield('login')
		@yield('error')
	@endif
	<!-- BEGIN MANDATORY SCRIPTS -->
	{{ HTML::script('assets/admin/plugins/jquery-1.11.js') }}
	{{ HTML::script('assets/admin/plugins/jquery-migrate-1.2.1.js') }}
	{{ HTML::script('assets/admin/plugins/jquery-ui/jquery-ui-1.10.4.min.js') }}
	{{ HTML::script('assets/admin/plugins/bootstrap/bootstrap.min.js') }}
	{{ HTML::script('assets/admin/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js') }}
	{{ HTML::script('assets/admin/plugins/bootstrap-select/bootstrap-select.js') }}
	{{ HTML::script('assets/admin/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}
	{{ HTML::script('assets/admin/plugins/mmenu/js/jquery.mmenu.min.all.js') }}
	{{ HTML::script('assets/admin/plugins/nprogress/nprogress.js') }}
	{{ HTML::script('assets/admin/plugins/charts-sparkline/sparkline.min.js') }}
	{{ HTML::script('assets/admin/plugins/breakpoints/breakpoints.js') }}
	{{ HTML::script('assets/admin/plugins/numerator/jquery-numerator.js') }}
	{{ HTML::script('assets/admin/plugins/icheck/icheck.js?v=1.0.2') }}
	<!-- END MANDATORY SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	{{ HTML::script('assets/admin/plugins/bootstrap-switch/bootstrap-switch.js') }}
	{{ HTML::script('assets/admin/plugins/bootstrap-progressbar/bootstrap-progressbar.js') }}
	{{ HTML::script('assets/admin/plugins/datatables/dynamic/jquery.dataTables.min.js') }}
	{{ HTML::script('assets/admin/plugins/datatables/dataTables.bootstrap.js') }}
	{{ HTML::script('assets/admin/plugins/datatables/dataTables.tableTools.js') }}
	{{ HTML::script('assets/admin/plugins/datatables/table.editable.html') }}
	{{ HTML::script('assets/admin/js/table_dynamic.js') }}
	{{ HTML::script('assets/admin/js/table_editable.js') }}
	{{ HTML::script('assets/admin/plugins/metrojs/metrojs.min.js') }}
	{{ HTML::script('assets/admin/plugins/fullcalendar/moment.min.js') }}
	{{ HTML::script('assets/admin/plugins/fullcalendar/fullcalendar.min.js') }}
	{{ HTML::script('assets/admin/plugins/simple-weather/jquery.simpleWeather.min.js') }}
	{{ HTML::script('assets/admin/plugins/google-maps/markerclusterer.js') }}
	{{ HTML::script('http://maps.google.com/maps/api/js?sensor=true') }}
	{{ HTML::script('assets/admin/plugins/google-maps/gmaps.js') }}
	{{ HTML::script('assets/admin/plugins/charts-flot/jquery.flot.js') }}
	{{ HTML::script('assets/admin/plugins/charts-flot/jquery.flot.animator.min.js') }}
	{{ HTML::script('assets/admin/plugins/charts-flot/jquery.flot.resize.js') }}
	{{ HTML::script('assets/admin/plugins/charts-flot/jquery.flot.time.min.js') }}
	{{ HTML::script('assets/admin/plugins/charts-morris/raphael.min.js') }}
	{{ HTML::script('assets/admin/plugins/charts-morris/morris.min.js') }}
	{{ HTML::script('assets/admin/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}
	{{ HTML::script('assets/admin/js/calendar.js') }}
	{{ HTML::script('assets/admin/plugins/backstretch/backstretch.min.js') }}
	{{ HTML::script('assets/admin/js/account.js') }}
	{{ HTML::script('assets/admin/plugins/datetimepicker/jquery.datetimepicker.js') }}
	{{ HTML::script('assets/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}
	{{ HTML::script('assets/admin/plugins/pickadate/picker.js') }}
	{{ HTML::script('assets/admin/plugins/pickadate/picker.date.js') }}
	{{ HTML::script('assets/admin/plugins/pickadate/picker.time.js') }}
	{{ HTML::script('assets/admin/plugins/icheck/custom.min.js?v=1.0.2') }}
	{{ HTML::script('assets/admin/plugins/bootstrap-switch/bootstrap-switch.js') }}
	{{ HTML::script('assets/admin/plugins/bootstrap-progressbar/bootstrap-progressbar.js') }}
	{{ HTML::script('assets/admin/js/form.js') }}
	{{ HTML::script('assets/admin/plugins/parsley/parsley.js') }}
    {{ HTML::script('assets/admin/plugins/parsley/parsley.extend.js') }}
    {{ HTML::script('assets/admin/plugins/noty/jquery.noty.js') }}
	{{ HTML::script('assets/admin/plugins/noty/packaged/jquery.noty.packaged.js') }}
	{{ HTML::script('assets/admin/js/blog.js') }}
	{{ HTML::script('assets/admin/plugins/cke-editor/ckeditor.js') }}
	{{ HTML::script('assets/admin/plugins/dropzone/dropzone.min.js') }}
	@if(Request::is('admin'))
		{{ HTML::script('assets/admin/js/dashboard.js') }}
	@endif
	<!-- END  PAGE LEVEL SCRIPTS -->
	{{ HTML::script('assets/admin/js/application.js') }}
	@if(Session::has('success'))
		<script type="text/javascript">
			noty({
				text        : {{ json_encode(Session::get('success')) }},
				type        : 'success',
				dismissQueue: true,
				timeout     : 10000,
				closeWith   : ['click'],
				layout      : 'topRight',
				theme       : 'relax',
				maxVisible  : 10,
				animation: {
					open: 'animated bounceInRight', // Animate.css class names
					close: 'animated bounceOutRight', // Animate.css class names
					easing: 'swing', // unavailable - no need
					speed: 1000 // unavailable - no need
				}
			});
		</script>
	@elseif(Session::has('info'))
		<script type="text/javascript">
			noty({
				text        : {{ json_encode(Session::get('info')) }},
				type        : 'information',
				dismissQueue: true,
				timeout     : 10000,
				closeWith   : ['click'],
				layout      : 'topRight',
				theme       : 'relax',
				maxVisible  : 10,
				animation: {
					open: 'animated bounceInRight', // Animate.css class names
					close: 'animated bounceOutRight', // Animate.css class names
					easing: 'swing', // unavailable - no need
					speed: 1000 // unavailable - no need
				}
			});
		</script>
	@elseif(Session::has('warning'))
		<script type="text/javascript">
			noty({
				text        : {{ json_encode(Session::get('warning')) }},
				type        : 'warning',
				dismissQueue: true,
				timeout     : 10000,
				closeWith   : ['click'],
				layout      : 'topRight',
				theme       : 'relax',
				maxVisible  : 10,
				animation: {
					open: 'animated bounceInRight', // Animate.css class names
					close: 'animated bounceOutRight', // Animate.css class names
					easing: 'swing', // unavailable - no need
					speed: 1000 // unavailable - no need
				}
			});
		</script>
	@elseif(Session::has('error'))
		<script type="text/javascript">
			noty({
				text        : {{ json_encode(Session::get('error')) }},
				type        : 'error',
				dismissQueue: true,
				timeout     : 10000,
				closeWith   : ['click'],
				layout      : 'topRight',
				theme       : 'relax',
				maxVisible  : 10,
				animation: {
					open: 'animated bounceInRight', // Animate.css class names
					close: 'animated bounceOutRight', // Animate.css class names
					easing: 'swing', // unavailable - no need
					speed: 1000 // unavailable - no need
				}
			});
		</script>
	@endif
</body>

</html>