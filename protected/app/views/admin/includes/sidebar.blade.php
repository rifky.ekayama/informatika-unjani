<!-- BEGIN MAIN SIDEBAR -->
<nav id="sidebar">
	<div id="main-menu">
		<ul class="sidebar-nav">
			<li class="current">
				<a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i><span class="sidebar-text">Dashboard</span></a>
			</li>
			<li>
				<a href="#"><i class="glyph-icon flaticon-pages"></i><span class="sidebar-text">Blogging</span><span class="fa arrow"></span></a>
				 <ul class="submenu collapse">
					<li>
						<a href="{{ url('admin/blog') }}"><span class="sidebar-text">Dashboard</span></a>
					</li>
					<li>
						<a href="{{ url('admin/blog/articles') }}"><span class="sidebar-text">Articles</span></a>
					</li>
				</ul>    
			</li>
			<li>
				<a href="#"><i class="glyph-icon flaticon-account"></i><span class="sidebar-text">User Controller</span><span class="fa arrow"></span></a>
				 <ul class="submenu collapse">
					<li>
						<a href="{{ url('admin/users/groups') }}"><span class="sidebar-text">User Group</span></a>
					</li>
					<li>
						<a href="{{ url('admin/users/users') }}"><span class="sidebar-text">User List</span></a>
					</li>
				</ul>    
			</li>
		</ul>
	</div>
	<div class="footer-widget">
		<img src="{{ asset('assets/admin/img/gradient.png') }}" alt="gradient effet" class="sidebar-gradient-img" />
		<div class="sidebar-footer clearfix">
			<a class="pull-left" href="profil.html" rel="tooltip" data-placement="top" data-original-title="Settings"><i class="glyph-icon flaticon-settings21"></i></a>
			<a class="pull-left toggle_fullscreen" href="#" rel="tooltip" data-placement="top" data-original-title="Fullscreen"><i class="glyph-icon flaticon-fullscreen3"></i></a>
			<a class="pull-left" href="lockscreen.html" rel="tooltip" data-placement="top" data-original-title="Lockscreen"><i class="glyph-icon flaticon-padlock23"></i></a>
			<a class="pull-left" href="{{ url('admin/logout') }}" rel="tooltip" data-placement="top" data-original-title="Logout"><i class="fa fa-power-off"></i></a>
		</div>
	</div>
</nav>
<!-- END MAIN SIDEBAR -->