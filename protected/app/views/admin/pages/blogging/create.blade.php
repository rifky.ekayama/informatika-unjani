@extends('admin.layouts.master')

@section('title')
	{{ $title }}
@stop

@section('content')
	<div class="page-title">
		<i class="icon-custom-left"></i>
		<h3><strong>New Post</strong></h3>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="tabcordion">
				<ul id="myTab" class="nav nav-tabs">
					<li class="active"><a href="#post_general" data-toggle="tab">General</a></li>
					<li><a href="#post_reviews" data-toggle="tab">Comments 
						<span class="m-l-10 badge badge-primary">5</span></a>
					</li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane fade active in" id="post_general">
						<div class="row">
							<div class="col-md-8 post-column-left">
								@if($errors->any())
									<div class="alert alert-danger fade in">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										{{ HTML::ul($errors->all()) }}
									</div>
								@endif
								<div class="form-group">
									<h5>Featured Images (various will create a slideshow automatically)</h5>
									{{ Form::open(array('url' => 'admin/blog/articles', 'class' => 'dropzone', 'files'=> true, 'parsley-validate')) }}
									<div class="fallback">
										<input name="file" type="file" multiple />
									</div>
								</div>
								<div class="form-group">
									<input name="title" type="text" class="form-control f-18" id="title" placeholder="Title of your article..." required parsley-minlength="3">
								</div>
								<div class="form-group">
									<textarea class="ckeditor" name="content" required>
									</textarea>
								</div>
							</div>
							<div class="col-md-4">
								<div class="post-column-right">
									<div class="form-group">
										<div>
											<h5 class="pull-left m-t-0">Categories <span class="asterisk">*</span></h5>
											<a href="#" class="pull-right c-gray"><i class="fa fa-plus"></i> <strong>Add New</strong></a>
										</div>
										<select name="categories[]" class="form-control" multiple title="Choose one or more" required>
											<optgroup label="Language">
												<option>HTML</option>
												<option>CSS</option>
												<option>Javascript</option>
											</optgroup>
											<optgroup label="Type">
												<option>Tutorial</option>
												<option>Update</option>
												<option>News</option>
											</optgroup>
										</select>
									</div>
									<div class="form-group">
										<h5>Tags <span class="asterisk">*</span></h5>
										<select name="tags[]" class="form-control" multiple required>
											<option data-content="<span class='label label-warning'>HTML</span>">HTML</option>
											<option data-content="<span class='label label-danger'>CSS</span>">CSS</option>
											<option data-content="<span class='label label-success'>PHP</span>">PHP</option>
											<option data-content="<span class='label label-primary'>Javascript</span>">Javascript</option>
										</select>
									</div>
									<div class="form-group">
										<h5>Status <span class="asterisk">*</span></h5>
										<select name="status" class="form-control" required>
											<option value="online" data-content="<span class='label label-success w-300'>Online</span>"></option>
											<option value="offline" data-content="<span class='label label-danger w-300'>Offline</span>">Offline</option>
											<option value="draft" data-content="<span class='label label-warning w-300'>Draft</span>">Draft</option>
										</select>
									</div>
									<h3>Optimize SEO</h3>
									<div class="form-group">
										<h5>Meta Title <span class="asterisk">*</span></h5>
										<input name="metaTitle" type="text" class="form-control" placeholder="Product 1 : buy your shirt with Pixit">
									</div>
									<div class="form-group">
										<h5>Meta Description <span class="asterisk">*</span></h5>
										<textarea name="metaDesc" rows="4" class="form-control" placeholder="Max 255 characters..."></textarea>
									</div>
									<div class="form-group">
										<h5>Meta Keywords</h5>
										<textarea name="metaKeyword" rows="6" class="form-control" placeholder="Max 1000 characters..."></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="post_reviews">
						<div class="row">
							<div class="col-md-12">
								<table id="post-review" class="table table-tools table-hover">
								<thead>
									<tr>
										<th style="min-width:70px"><strong>Review ID</strong>
										<th><strong>Review Date</strong></th>
										<th><strong>User / Client</strong></th>
										<th><strong>Review Content</strong></th>
										<th class="text-center"><strong>Status</strong></th>
										<th class="text-center"><strong>Actions</strong></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>03/11/2013</td>
										<td>John Addams</td>
										<td>Good article. I like it a lot. Thanks! </td>
										<td class="text-center">
											<span class="label label-success w-300">Approved</span>
										</td>
										<td class="text-center"> 
											<a href="ecommerce_product_view.html" class="edit btn btn-sm btn-default"><i class="fa fa-pencil"></i> Edit</a>
											<a href="#" class="delete btn btn-sm btn-default"><i class="fa fa-times-circle"></i> Remove</a>
										</td>
									</tr>
									<tr>
										<td>2</td>
										<td>02/11/2013</td>
										<td>Fred Aster</td>
										<td>I sell my car. Do you want to buy it?</td>
										<td class="text-center">
											<span class="label label-danger w-300">Rejected</span>
										</td>
										<td class="text-center"> 
											<a href="ecommerce_product_view.html" class="edit btn btn-sm btn-default"><i class="fa fa-pencil"></i> Edit</a>
											<a href="#" class="delete btn btn-sm btn-default"><i class="fa fa-times-circle"></i> Remove</a>
										</td>
									</tr>
									<tr>
										<td>3</td>
										<td>01/11/2013</td>
										<td>Mike Johson</td>
										<td>Very interesting. Thanks for sharing! </td>
										<td class="text-center">
											<span class="label label-info w-300">Pending</span>
										</td>
										<td class="text-center"> 
											<a href="ecommerce_product_view.html" class="edit btn btn-sm btn-default"><i class="fa fa-pencil"></i> Edit</a>
											<a href="#" class="delete btn btn-sm btn-default"><i class="fa fa-times-circle"></i> Remove</a>
										</td>
									</tr>
									<tr>
										<td>4</td>
										<td>25/10/2013</td>
										<td>Amanda Taping</td>
										<td>Love it. </td>
										<td class="text-center">
											<span class="label label-success w-300">Approved</span>
										</td>
										<td class="text-center"> 
											<a href="ecommerce_product_view.html" class="edit btn btn-sm btn-default"><i class="fa fa-pencil"></i> Edit</a>
											<a href="#" class="delete btn btn-sm btn-default"><i class="fa fa-times-circle"></i> Remove</a>
										</td>
									</tr>
									<tr>
										<td>5</td>
										<td>20/10/2013</td>
										<td>John Johnson</td>
										<td>I don't understand anything</td>
										<td class="text-center">
											<span class="label label-success w-300">Approved</span>
										</td>
										<td class="text-center"> 
											<a href="ecommerce_product_view.html" class="edit btn btn-sm btn-default"><i class="fa fa-pencil"></i> Edit</a>
											<a href="#" class="delete btn btn-sm btn-default"><i class="fa fa-times-circle"></i> Remove</a>
										</td>
									</tr>
								</tbody>
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 m-t-20 m-b-40 align-center">
			<button class="btn btn-default m-r-10 m-t-10"><i class="fa fa-reply"></i> Cancel</button>
			<button class="btn btn-default m-r-10 m-t-10"><i class="fa fa-edit"></i> Save as Draft</button>
			<button type="submit" onclick="javascript:$('#form1').parsley('validate');" class="btn btn-success m-t-10"><i class="fa fa-check"></i> Save &amp; Publish</button>
			{{ Form::close() }}
		</div>
	</div>
@stop