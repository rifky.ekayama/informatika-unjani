@extends('admin.layouts.master')

@section('title')
	{{ $title }}
@stop

@section('content')
	<div class="top-page clearfix">
		<div class="page-title pull-left">
			<h3 class="pull-left"><strong>Manage Articles</strong></h3>
		</div>
		 <div class="pull-right">
			<a href="{{ url('admin/blog/articles/create') }}" class="btn btn-primary m-t-10"><i class="fa fa-plus p-r-10"></i> Add a Post</a>
		</div>
	</div>
	<div class="top-menu">
		<a href="#"><strong>All</strong></a><span class="label label-default m-l-10">112</span> <span class="c-gray p-l-10 p-r-5">|</span>
		<a href="#">Draft</a><span class="label label-default m-l-10">18</span> <span class="c-gray p-l-10 p-r-5">|</span>
		<a href="#">Deleted</a><span class="label label-default m-l-10">42</span> <span class="c-gray p-l-10 p-r-5">|</span>
		<a href="#">Scheduled</a><span class="label label-default m-l-10">4</span>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
			<table class="table table-striped dataTable" aria-describedby="example2_info">
				<thead>
					<tr role="row">
						<th>Title</th>
						<th>Author</th>
						<th>Categories</th>
						<th>Tags</th>
						<th>Creation</th>
						<th>Comments</th>
						<th class="text-center">Status</th>
						<th width="13%">Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($posts as $value)
						<tr>
							<td>{{ $value->title }}</td>
							<td>{{ $value->user->first_name }}</td>
							<td>
								{{--*/ $category = json_decode($value->categories, true) /*--}}
								@for($i=0;$i<count($category);$i++)
									@if($i == count($category)-1)
										{{ $category[$i] }}
									@else
										{{ $category[$i]."," }}
									@endif
								@endfor
							</td>
							<td>
								{{--*/ $tag = json_decode($value->tags, true) /*--}}
								@for($i=0;$i<count($tag);$i++)
									<span class="label label-default"><i class="fa fa-tag f-10 p-r-5 c-gray-light"></i> {{ $tag[$i] }}</span> 
								@endfor
							</td>
							<td>{{ date("d-m-Y, H:i", strtotime($value->created_at))}}</td>
							<td>{{ $value->comment_count }}</td>
							<td class="text-center">
								@if($value->status == "online")
									<span class="label label-success w-300">Online</span>
								@elseif($value->status == "offline")
									<span class="label label-danger w-300">Offline</span>
								@elseif($value->status == "draft")
									<span class="label label-warning w-300">Draft</span>
								@endif
							</td>
							<td>
								<a href="{{ URL::to('admin/blog/articles/'.Crypt::encrypt($value->id).'/edit') }}" class="edit btn btn-sm btn-icon btn-rounded btn-default"><i class="fa fa-pencil"></i></a>
								<a href="{{ URL::to('admin/blog/articles/delete/'.Crypt::encrypt($value->id)) }}" class="delete btn btn-sm btn-icon btn-rounded btn-default"><i class="fa fa-times"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop