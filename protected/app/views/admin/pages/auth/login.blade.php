@extends('admin.layouts.master')

@section('title')
	{{ $title }}
@stop

@section('login')
	<div class="container" id="login-block">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
				<div class="login-box clearfix animated flipInY">
					<div class="page-icon animated bounceInDown">
						<img src="{{ asset('assets/admin/img/account/user-icon.png') }}" alt="Key icon">
					</div>
					<div class="login-logo">
						<a href="#?login-theme-3">
							<img src="{{ asset('assets/admin/img/account/login-logo.png') }}" alt="Company Logo">
						</a>
					</div>
					<hr>
					<div class="login-form">
						<!-- BEGIN ERROR BOX -->
						<div class="alert alert-danger hide">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<h4>Error!</h4>
							Your Error Message goes here
						</div>
						<!-- END ERROR BOX -->
						{{ Form::open(array('url' => 'admin/login', 'class' => 'form-horizontal', 'id' => 'form1', 'parsley-validate')) }}
							{{ Form::text('email', null, array('class'=>'input-field form-control user', 'placeholder'=>'Email', 'parsley-type' => 'email', 'required')) }}
							{{ Form::password('password', array('class'=>'input-field form-control password', 'placeholder'=>'Password', 'parsley-minlength' => '8', 'required')) }}
							{{ Form::submit('Login', array('class'=>'btn btn-login', 'onclick' => 'javascript:$("#form1").parsley("validate");')) }}
						{{ Form::close() }}
						<div class="login-links">
							<a href="{{ URL::to('admin/login/forgotPassword') }}">Forgot password?</a>
							<br>
							<a href="#">Don't have an account? <strong>Sign Up</strong></a>
						</div>
					</div>
				</div>
				<div class="social-login row">
					<div class="fb-login col-lg-6 col-md-12 animated flipInX">
						<a href="#" class="btn btn-facebook btn-block">Connect with <strong>Facebook</strong></a>
					</div>
					<div class="twit-login col-lg-6 col-md-12 animated flipInX">
						<a href="#" class="btn btn-twitter btn-block">Connect with <strong>Twitter</strong></a>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop