@extends('admin.layouts.master')

@section('title')
	{{ $title }}
@stop

@section('login')
	<!-- START SIGNUP BOX -->
	<div class="container" id="login-block">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
				<div class="login-box clearfix animated flipInY">
					<div class="page-icon animated bounceInDown">
						<img src="{{ asset('assets/admin/img/account/register-icon.png') }}" alt="Register icon" />
					</div>
					<div class="login-logo">
						<a href="#">
							<img src="{{ asset('assets/admin/img/account/login-logo.png') }}" alt="Company Logo" />
						</a>
					</div>
					<hr>
					<div class="login-form">
						<!-- Start Error box -->
						<div class="alert alert-danger hide">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<h4>Error!</h4>
							Your Error Message goes here
						</div>
						<!-- End Error box -->
						{{ Form::open(array('url' => 'admin/login/doForgotPassword', 'class' => 'form-horizontal', 'id' => 'form1', 'parsley-validate')) }}
							{{ Form::text('email', null, array('class'=>'input-field form-control user', 'placeholder'=>'Email', 'parsley-type' => 'email', 'required')) }}
							{{ Form::password('password', array('id' => 'password', 'class'=>'input-field form-control password', 'placeholder'=>'Password', 'parsley-minlength' => '8', 'required')) }}
							{{ Form::password('confirmPassword', array('placeholder' => 'Confirm Password', 'class' => 'input-field','parsley-equalto' => '#password','required')) }}
							{{ Form::hidden('code', $code) }}
							{{ Form::hidden('id', $id) }}
							{{ Form::submit('Reset Password', array('class'=>'btn btn-login', 'onclick' => 'javascript:$("#form1").parsley("validate");')) }}
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END SIGNUP BOX -->
@stop