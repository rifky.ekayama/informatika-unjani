@extends('admin.layouts.master')

@section('title')
	{{ $title }}
@stop

@section('login')
	<div class="container" id="login-block">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
				<div class="login-box clearfix animated flipInY">
					<div class="page-icon animated bounceInDown">
						<img src="{{ asset('assets/admin/img/account/login-questionmark-icon.png') }}" alt="Questionmark icon" />
					</div>
					<div class="login-logo">
						<a href="#">
							<img class="img-responsive" src="{{ asset('assets/admin/img/account/login-logo.png') }}" alt="Company Logo" />
						</a>
					</div>
					<hr />
					<div class="login-form">
						<!-- BEGIN ERROR BOX -->
						<div class="alert alert-danger hide">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<h4>Error!</h4>
							Your Error Message goes here
						</div>
						<!-- END ERROR BOX -->
						{{ Form::open(array('url' => 'admin/login/forgotPassword', 'id' => 'form1', 'parsley-validate')) }}
							<p>Enter your email address below and we'll send a special reset password link to your inbox.</p>
							{{ Form::text('email', null, array('class'=>'input-field form-control user', 'placeholder'=>'Email', 'parsley-type' => 'email', 'required')) }}
							{{ Form::submit('Reset password', array('class'=>'btn btn-login btn-reset', 'onclick' => 'javascript:$("#form1").parsley("validate");')) }}
						{{ Form::close() }}
						<div class="login-links">
							<a href="{{ url('admin/login') }}">Already have an account?  <strong>Sign In</strong></a>
							<br>
							<a href="#"> Don't have an account? <strong>Sign Up</strong></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop