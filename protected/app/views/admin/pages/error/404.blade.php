@extends('admin.layouts.master')

@section('title')
	404
@stop

@section('error')
	<div class="row">
		<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-offset-1 col-xs-10">
			<div class="error-container">
				<div class="error-main">
					<h1> Oops.</h1>
					<h3> The page you are trying to reach doesn't exist. </h3>
					<h4> Go back to our site or <a href="email_compose.html">contact us</a> about the problem. </h4>
					<br>
					<div class="row">
						<div class="input-icon col-md-12">
							<i class="fa fa-search"></i>
							<input type="text" class="form-control" placeholder="Search for page">
						</div>
					</div>
					<br>
					<button class="btn btn-dark" type="button">Search</button>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="copyright">© Copyright Pixit, 2014 Pixit Inc.</div>
	</div>
@stop