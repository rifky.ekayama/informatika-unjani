@extends('admin.layouts.master')

@section('title')
	{{ $title }}
@stop

@section('content')
	<div class="page-title"> <i class="icon-custom-left"></i>
		<h3><strong>Create</strong> User</h3>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><strong>Create</strong> User</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							{{ Form::open(array('url' => 'admin/users/users', 'id' => 'form1', 'class' => 'form-horizontal', 'parsley-validate')) }}
								<div class="form-group">
									<label class="col-sm-2 control-label">Email <span class="asterisk">*</span></label>
									<div class="col-sm-10">
										{{ Form::text('email', Input::old('email'), array('placeholder' => 'Email', 'class' => 'form-control', 'parsley-type' => 'email', 'required')) }}
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Password <span class="asterisk">*</span></label>
									<div class="col-sm-10">
										<div class="controls">
											{{ Form::password('password', array( 'id' => 'password', 'placeholder' => 'Password', 'class' => 'form-control', 'parsley-minlength' => '8', 'required')) }}
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Confirm Password <span class="asterisk">*</span></label>
									<div class="col-sm-10">
										<div class="controls">
											{{ Form::password('confirmPassword', array('placeholder' => 'Confirm Password', 'class' => 'form-control','parsley-equalto' => '#password','required')) }}
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">First Name </label>
									<div class="col-sm-10">
										{{ Form::text('firstName', Input::old('firstName'), array('placeholder' => 'Email', 'class' => 'form-control', 'parsley-minlength' => '3')) }}
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Last Name </label>
									<div class="col-sm-10">
										{{ Form::text('lastName', Input::old('lastName'), array('placeholder' => 'Email', 'class' => 'form-control')) }}
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">User Group <span class="asterisk">*</span></label>
									<div class="col-sm-10">
										{{ Form::select('group', $group, null, array('class' => 'form-control', 'data-live-search' => 'true', 'required')) }}
									</div>
								</div>
								<div class="col-sm-10 col-sm-offset-2">
									<div class="pull-right">
										<button class="btn btn-primary m-b-10" onclick="javascript:$('#form1').parsley('validate');">Submit</button>
										<button type="reset" class="btn btn-default m-b-10">Cancel</button>
									</div>
								</div>
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop