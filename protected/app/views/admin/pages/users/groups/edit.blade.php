@extends('admin.layouts.master')

@section('title')
	{{ $title }}
@stop

@section('content')
	<div class="page-title"> <i class="icon-custom-left"></i>
		<h3><strong>Create</strong> Group</h3>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><strong>Create</strong> Group</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							{{--*/ $perm = json_decode($groupbyid->permissions, true) /*--}}
							{{ Form::model($groupbyid, array('route' => array('admin.users.groups.update', Crypt::encrypt($groupbyid->id)),'method'=>'PUT', 'id' => 'form1', 'class' => 'form-horizontal', 'parsley-validate')) }}
								<div class="form-group">
									<label class="col-sm-2 control-label">Group Name <span class="asterisk">*</span>
									</label>
									<div class="col-sm-10">
										{{ Form::text('name', $groupbyid->name, array('placeholder' => 'type Group Name', 'class' => 'form-control')) }}
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">User Controll</label>
									<div class="col-sm-10">
										<div class="skin-section">
											<ul class="list">
												<li>
													{{ Form::checkbox('cb[user.read]', '1', (!empty($perm['user.read']) == 1 ? true : false)) }}
													<label for="flat-checkbox-1">Read</label>
												</li>
												<li>
													{{ Form::checkbox('cb[user.create]', '1', (!empty($perm['user.create']) == 1 ? true : false)) }}
													<label for="flat-checkbox-1">Create</label>
												</li>
												<li>
													{{ Form::checkbox('cb[user.update]', '1', (!empty($perm['user.update']) == 1 ? true : false)) }}
													<label for="flat-checkbox-1">Update</label>
												</li>
												<li>
													{{ Form::checkbox('cb[user.delete]', '1', (!empty($perm['user.delete']) == 1 ? true : false)) }}
													<label for="flat-checkbox-1">Delete</label>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Blogging</label>
									<div class="col-sm-10">
										<div class="skin-section">
											<ul class="list">
												<li>
													{{ Form::checkbox('cb[blogging.read]', '1', (!empty($perm['blogging.read']) == 1 ? true : false)) }}
													<label for="flat-checkbox-1">Read</label>
												</li>
												<li>
													{{ Form::checkbox('cb[blogging.create]', '1', (!empty($perm['blogging.create']) == 1 ? true : false)) }}
													<label for="flat-checkbox-1">Create</label>
												</li>
												<li>
													{{ Form::checkbox('cb[blogging.update]', '1', (!empty($perm['blogging.update']) == 1 ? true : false)) }}
													<label for="flat-checkbox-1">Update</label>
												</li>
												<li>
													{{ Form::checkbox('cb[blogging.delete]', '1', (!empty($perm['blogging.delete']) == 1 ? true : false)) }}
													<label for="flat-checkbox-1">Delete</label>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-sm-10 col-sm-offset-2">
									<div class="pull-right">
										<button class="btn btn-primary m-b-10" onclick="javascript:$('#form1').parsley('validate');">Submit</button>
										<button type="reset" class="btn btn-default m-b-10">Cancel</button>
									</div>
								</div>
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop