@extends('admin.layouts.master')

@section('title')
	{{ $title }}
@stop

@section('content')
	<div class="page-title"> <i class="icon-custom-left"></i>
		<h3><strong>User</strong> Group</h3>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-red">
					<h3 class="panel-title"><strong>Group </strong> List</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12 m-b-20">
							<div class="btn-group">
								<a href="{{ url('admin/users/groups/create') }}">
									<button class="btn btn-danger">
										Add New <i class="fa fa-plus"></i>
									</button>
								</a>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-hover table-dynamic">
								<thead>
									<tr>
										<th width="70%">Group Name</th>
										<th width="30$">Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach($group as $value)
										<tr>
											<td>{{ $value->name }}</td>
											<td class="text-center">
												<a class="edit btn btn-dark" href="{{ URL::to('admin/users/groups/'.Crypt::encrypt($value->id).'/edit') }}"><i class="fa fa-pencil-square-o"></i>Edit</a>
												<a class="delete btn btn-danger" href="{{ URL::to('admin/users/groups/delete/'.Crypt::encrypt($value->id))}}"><i class="fa fa-times-circle"></i> Remove</a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop